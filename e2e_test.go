package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"reflect"
	"sort"
	"testing"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/prometheus/common/model"
	"github.com/prometheus/tsdb"
	"github.com/prometheus/tsdb/labels"
	v1 "gitlab.com/gitlab-org/prometheus-storage-migrator/v1storage"
)

const (
	numInstances      = 10
	seriesPerInstance = 10
	lookback          = 48 * time.Hour
	interval          = 15 * time.Second
	step              = 30 * time.Second
)

func TestE2E(t *testing.T) {
	v1Path, err := ioutil.TempDir("", "test_storage")
	if err != nil {
		t.Fatalf("Opening test dir failed: %s", err)
	}
	defer os.RemoveAll(v1Path)

	v1Storage := v1.NewMemorySeriesStorage(&v1.MemorySeriesStorageOptions{
		TargetHeapSize:             2000000000,
		PersistenceRetentionPeriod: 999999 * time.Hour,
		PersistenceStoragePath:     v1Path,
		HeadChunkTimeout:           999999 * time.Hour,
		CheckpointInterval:         999999 * time.Hour,
		CheckpointDirtySeriesLimit: 1e9,
		MinShrinkRatio:             0.5,
		SyncStrategy:               v1.Never,
	})
	if err := v1Storage.Start(); err != nil {
		t.Fatal(err)
	}

	var writtenSamples model.Matrix

	endTime := model.Now()
	for inst := 0; inst < numInstances; inst++ {
		for ser := 0; ser < seriesPerInstance; ser++ {
			sample := &model.Sample{
				Metric: model.Metric{
					model.MetricNameLabel: "testmetric",
					model.InstanceLabel:   model.LabelValue(fmt.Sprintf("instance-%d", inst)),
					"testlabel":           model.LabelValue(fmt.Sprintf("series-%d", ser)),
				},
			}
			ss := &model.SampleStream{
				Metric: sample.Metric,
			}
			for ts := endTime.Add(-lookback); !ts.After(endTime); ts = ts.Add(interval) {
				sample.Timestamp = ts
				sample.Value = model.SampleValue(rand.Float64() * 1000)
				err := v1Storage.Append(sample)
				if err != nil {
					t.Fatal(err)
				}

				ss.Values = append(ss.Values, model.SamplePair{Timestamp: sample.Timestamp, Value: sample.Value})
			}

			writtenSamples = append(writtenSamples, ss)
		}
	}

	v1Storage.Stop()

	v2Path, err := ioutil.TempDir("", "test_storage")
	if err != nil {
		t.Fatalf("Opening test dir failed: %s", err)
	}
	defer os.RemoveAll(v2Path)
	v2Storage, err := tsdb.Open(v2Path, log.NewNopLogger(), nil, &tsdb.Options{
		WALFlushInterval:  5 * time.Second,
		RetentionDuration: 999999 * 24 * 60 * 60 * 1000,
		BlockRanges:       []int64{2 * 60 * 60 * 1000},
	})
	if err != nil {
		t.Fatalf("Error starting v2 storage: %s", err)
	}
	defer v2Storage.Close()

	migrator := storageMigrator{
		v1Path:      v1Path,
		v2Storage:   v2Storage,
		parallelism: 8,
	}
	if err := migrator.migrate(context.Background(), lookback+time.Hour, step); err != nil {
		t.Fatalf("Error migrating storage: %s", err)
	}

	q, err := v2Storage.Querier(0, int64(endTime))
	if err != nil {
		t.Fatalf("Error creating querier: %s", err)
	}
	defer q.Close()

	m, err := labels.NewRegexpMatcher("instance", ".+")
	if err != nil {
		t.Fatalf("Error creating matcher: %s", err)
	}
	set, err := q.Select(m)

	var readSamples model.Matrix
	for set.Next() {
		series := set.At()

		m := make(model.Metric, len(series.Labels()))
		for _, l := range series.Labels() {
			m[model.LabelName(l.Name)] = model.LabelValue(l.Value)
		}

		ss := &model.SampleStream{
			Metric: m,
		}

		it := series.Iterator()
		for it.Next() {
			t, v := it.At()
			ss.Values = append(ss.Values, model.SamplePair{Timestamp: model.Time(t), Value: model.SampleValue(v)})
		}
		if it.Err() != nil {
			t.Fatalf("error querying v2 storage: %s", err)
		}

		readSamples = append(readSamples, ss)
	}
	if set.Err() != nil {
		t.Fatalf("error querying v2 storage: %s", err)
	}

	sort.Sort(writtenSamples)
	sort.Sort(readSamples)

	if !reflect.DeepEqual(readSamples, writtenSamples) {
		t.Fatalf("unexpected samples; want %v, got %v", writtenSamples, readSamples)
	}
}
